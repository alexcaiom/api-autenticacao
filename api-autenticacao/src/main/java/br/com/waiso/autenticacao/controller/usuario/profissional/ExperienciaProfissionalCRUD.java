package br.com.waiso.autenticacao.controller.usuario.profissional;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.waiso.autenticacao.orm.dao.finder.usuario.profissional.IFinderCargo;
import br.com.waiso.autenticacao.orm.dao.finder.usuario.profissional.IFinderEmpresa;
import br.com.waiso.autenticacao.orm.dao.usuario.profissional.IExperienciaProfissionalDAO;
import br.com.waiso.autenticacao.orm.modelo.UsuarioNivelVisualizacao;
import br.com.waiso.autenticacao.orm.modelo.usuario.profissional.Cargo;
import br.com.waiso.autenticacao.orm.modelo.usuario.profissional.Empresa;
import br.com.waiso.autenticacao.orm.modelo.usuario.profissional.ExperienciaProfissional;

@Controller
@RequestMapping("/experienciaProfissional")
public class ExperienciaProfissionalCRUD {

	@Autowired
	IFinderCargo finderCargo;
	@Autowired
	IFinderEmpresa finderEmpresa;
	@Autowired
	IExperienciaProfissionalDAO dao;
	
	@RequestMapping(value="/incluir", method=RequestMethod.PUT)
	public void incluir (@RequestParam("idCargo") Long idCargo, @RequestParam("idCargo") Long idEmpresa, @RequestParam("dataInicio") GregorianCalendar dataInicio, @RequestParam("dataFim") GregorianCalendar dataFim) {
		ExperienciaProfissional experienciaProfissional = new ExperienciaProfissional();
		Cargo cargo = finderCargo.pesquisar(idCargo);
		experienciaProfissional.setCargo(cargo);
		
		Empresa empresa = finderEmpresa.pesquisar(idEmpresa);
		experienciaProfissional.setEmpresa(empresa);
		
		Calendar inicio = dataInicio;
		experienciaProfissional.setInicio(inicio);
		
		Calendar fim = dataFim;
		experienciaProfissional.setFim(fim);
		UsuarioNivelVisualizacao nivelDeAcesso = null;
		experienciaProfissional.setNivelDeAcesso(nivelDeAcesso);
		experienciaProfissional = dao.incluir(experienciaProfissional);
		
	}
	
	@RequestMapping("/listar")
	public List<Cargo> listar () {
		List<Cargo> cargos = finderCargo.listar();
		return cargos;
	}
	
	@RequestMapping(value="/pesquisar", method=RequestMethod.GET)
	public Cargo pesquisar (long id) {
		Cargo cargo = finderCargo.pesquisar(id);
		return cargo;
	}
	
	@RequestMapping(value="/pesquisarPorNomeComo", method=RequestMethod.GET)
	public List<Cargo> pesquisarPorNomeComo (@RequestParam("nome") String nome) {
		List<Cargo> cargos = finderCargo.pesquisarPorNomeComo(nome);
		return cargos;
	}
	
}
