package br.com.waiso.autenticacao.controller;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

//import org.apache.tomcat.util.http.fileupload.FileItem;
//import org.apache.tomcat.util.http.fileupload.disk.DiskFileItemFactory;
//import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;
import org.springframework.beans.factory.annotation.Autowired;

//import br.com.waiso.autenticacao.dto.UsuarioDTO;
//import br.com.waiso.autenticacao.dto.mapeadores.UsuarioMapeador;
import br.com.waiso.autenticacao.excecoes.ErroNegocio;
import br.com.waiso.autenticacao.orm.bo.BOUsuario;
import br.com.waiso.autenticacao.orm.bo.BOUsuarioImpl;
import br.com.waiso.autenticacao.orm.modelo.Foto;
import br.com.waiso.autenticacao.orm.modelo.Perfil;
import br.com.waiso.autenticacao.orm.modelo.usuario.Usuario;
import br.com.waiso.autenticacao.utils.Constantes;
import br.com.waiso.autenticacao.utils.SessaoUsuario;
import br.com.waiso.autenticacao.utils.SessoesUsuario;
import br.com.waiso.framework.abstratas.Classe;
import br.com.waiso.framework.exceptions.ErroUsuario;
import br.com.waiso.framework.json.JSONReturn;

public class UsuarioCRUD {
	
	private static final String CAMPO_NOME = "nome";
	private static final String CAMPO_LOGIN = "login";
	private static final String CAMPO_SENHA = "senha";
	private static final String CAMPO_EMAIL = "email";
	private boolean isMultiPart;
	private String caminhoArquivo;
	private int maxFileSize = 50 * 1024;
	private int maxMemSize = 4* 1024;
	private File arquivo;

	public UsuarioCRUD() {
//		setCamposObrigatorios(CAMPO_LOGIN, CAMPO_NOME, CAMPO_SENHA, CAMPO_EMAIL);
	}
	
	@Autowired
	BOUsuario bo;

	/*public JSONReturn incluir(ServletRequest requisicao, ServletResponse resposta) {
		validaCamposObrigatorios(requisicao, resposta);
		
		List<Foto> fotos = null;
		
		isMultiPart = ServletFileUpload.isMultipartContent((HttpServletRequest) requisicao);
		if (isMultiPart) {
			fotos = new ArrayList<>();
			try {
				String caminhoArquivoTemp = getServletContext().getRealPath("/temp/");
				if (!new File(caminhoArquivoTemp).exists()) {
					new File(caminhoArquivoTemp).mkdirs();
				}
				DiskFileItemFactory fabrica = new DiskFileItemFactory(maxMemSize, new File(caminhoArquivoTemp));

				ServletFileUpload upload = new ServletFileUpload(fabrica);
				try{
					List<FileItem> arquivos = upload.parseRequest((HttpServletRequest) requisicao);
					Iterator<FileItem> i = arquivos.iterator();

					caminhoArquivo = getServletContext().getRealPath("/storage/");
					if (!new File(caminhoArquivo).exists()) {
						new File(caminhoArquivo).mkdirs();
					}
					caminhoArquivo = caminhoArquivo + "/";

					while (i.hasNext()) {
						FileItem item = i.next();
						if (!item.isFormField()) {
							String nomeCampo = item.getFieldName();
							String nomeArquivo = item.getName();
							String tipoConteudo = item.getContentType();
							boolean estaEmMemoria = item.isInMemory();
							long tamanhoEmBytes = item.getSize();

							if (nomeArquivo.lastIndexOf("/") >= 0) {
								arquivo = new File(caminhoArquivo + nomeArquivo.substring(nomeArquivo.lastIndexOf("\\")));
							} else {
								arquivo = new File(caminhoArquivo + nomeArquivo.substring(nomeArquivo.lastIndexOf("\\")+1));
							}

							item.write(arquivo);
							Foto foto = new Foto();
							foto.setCaminho(arquivo.getAbsolutePath());
							foto.setData(GregorianCalendar.getInstance());
							foto.setDescricao(arquivo.getName());
							fotos.add(foto);
						} else {
						}
					}
				} catch (Exception e) {
					getWriter(null, resposta).println("Erro: "+e.getMessage());
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		Usuario u = new Usuario();
		String login = 		getStringDaRequisicao(requisicao, CAMPO_LOGIN);
		
		u.setLogin(login);
		String nome = 		getStringDaRequisicao(requisicao, CAMPO_NOME);
		u.setNome(nome);
		String senha =		getStringDaRequisicao(requisicao, CAMPO_SENHA);
		u.setSenha(senha);
		String email = 		getStringDaRequisicao(requisicao, CAMPO_EMAIL);
		u.setEmail(email);
		
		Long idPerfil = 0l; 
		String strIdPerfil = getStringDaRequisicao(requisicao, "idPerfil");
		Perfil perfil = new Perfil();
		perfil.setId(2l);
		if (existe(strIdPerfil)) {
			idPerfil = Long.parseLong(strIdPerfil);
			perfil.setId(idPerfil);
		}
		
		u.setPerfil(perfil);

		u = bo.incluir(u);
		
		return SUCESSO(u); 
	}*/
	
	/*public JSONReturn autenticar(ServletRequest requisicao, ServletResponse resposta) {
		String login = getStringDaRequisicao(requisicao, CAMPO_LOGIN);
		String senha = getStringDaRequisicao(requisicao, CAMPO_SENHA);
		Usuario usuario = null;
		try {
			usuario = bo.autentica(login, senha);
			
			adicionarSessaoDeUsuario(requisicao, usuario);
		} catch (ErroNegocio e) {
			return ATENCAO(e.getErro());
		}
		return SUCESSO(usuario, "dataDeNascimento", "dataDeCriacao");
	}*/
	
	/*public JSONReturn usuarioEstaLogado(ServletRequest requisicao, ServletResponse resposta) {
		String login = getStringDaRequisicao(requisicao, CAMPO_LOGIN);
		try {
			SessoesUsuario sessoes = getSessoesUsuario(requisicao);
			boolean usuarioLogado = sessoes.tem(login);
			if (usuarioLogado) {
				boolean valida = sessoes.valida(login);
				return SUCESSO(valida);
			}
		} catch (ErroNegocio e) {
			return ATENCAO(e.getErro());
		}
		return SUCESSO(false);
	}*/
	
	/*private SessoesUsuario adicionarSessaoDeUsuario(ServletRequest requisicao, Usuario usuario) {
		SessoesUsuario sessoes = getSessoesUsuario(requisicao);
		
		SessaoUsuario sessaoDeUsuario = new SessaoUsuario(usuario.getLogin(), usuario, GregorianCalendar.getInstance());
		sessoes.add(usuario.getLogin(), sessaoDeUsuario);
		return sessoes;
	}*/

	/*private SessoesUsuario getSessoesUsuario(ServletRequest requisicao) {
		HttpSession sessao = ((HttpServletRequest)requisicao).getSession();
		SessoesUsuario sessoes = (SessoesUsuario) sessao.getAttribute(Constantes.USUARIO);
		
		if (Classe.naoExiste(sessoes)) {
			sessoes = new SessoesUsuario();
			sessao.setAttribute(Constantes.USUARIO, sessoes);
		}
		return sessoes;
	}*/

	/*public JSONReturn listar(ServletRequest requisicao, ServletResponse resposta) {
		List<Usuario> usuarios = null;
		try {
			 usuarios = bo.listarUsuarios();
		} catch(Exception e) {
			throw new ErroUsuario(e.getCause().getMessage());
		}
		List<UsuarioDTO> usuariosDTO = UsuarioMapeador.from(usuarios);
		return SUCESSO(usuariosDTO, "dataDeNascimento", "dataDeCriacao"); 
	}*/
	
	/*public JSONReturn pesquisarPorNome(ServletRequest requisicao, ServletResponse resposta) {
		String nome = getStringDaRequisicao(requisicao, CAMPO_NOME);
		List<Usuario> usuarios = bo.pesquisarPorNome(nome);
		List<UsuarioDTO> usuariosDTO = UsuarioMapeador.from(usuarios);
		return SUCESSO(usuariosDTO, "dataDeNascimento", "dataDeCriacao"); 
	}*/
	
}
