package br.com.waiso.autenticacao.controller.usuario.profissional;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.waiso.autenticacao.orm.dao.finder.impl.usuario.profissional.FinderCargoImpl;
import br.com.waiso.autenticacao.orm.dao.finder.usuario.profissional.IFinderCargo;
import br.com.waiso.autenticacao.orm.dao.usuario.profissional.ICargoDAO;
import br.com.waiso.autenticacao.orm.modelo.usuario.profissional.Cargo;

@Controller
@RequestMapping("/cargo")
public class CargoCRUD {

	@Autowired
	ICargoDAO dao;
	@Autowired
	IFinderCargo finderCargo;
	
	@RequestMapping(value="/incluir", method=RequestMethod.POST)
	public void incluir (String nome) {
		Cargo cargo = new Cargo();
		cargo.setNome(nome);
		cargo = dao.incluir(cargo);
	}
	
	@RequestMapping(value="/listar", method=RequestMethod.GET)
	public List<Cargo> listar () {
		List<Cargo> cargos = finderCargo.listar();
		return (cargos);
	}
	
	@RequestMapping(value="/pesquisar", method=RequestMethod.GET)
	public Cargo pesquisar (@RequestParam("id") Long id) {
		Cargo cargo = new FinderCargoImpl().pesquisar(id);
		return cargo;
	}
	
	@RequestMapping(value="/pesquisarPorNomeComo", method=RequestMethod.GET)
	public List<Cargo> pesquisarPorNomeComo (@RequestParam("nome") String nome) {
		List<Cargo> cargos = finderCargo.pesquisarPorNomeComo(nome);
		return cargos;
	}
	
}
