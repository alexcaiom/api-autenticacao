package br.com.waiso.autenticacao.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.waiso.autenticacao.orm.dao.ISistemaDAO;
import br.com.waiso.autenticacao.orm.dao.finder.IFinderSistema;
import br.com.waiso.autenticacao.orm.dao.finder.impl.FinderSistemaImpl;
import br.com.waiso.autenticacao.orm.modelo.sistema.Sistema;

@Controller
@RequestMapping(value="/sistema")
public class SistemaCRUD {

	@Autowired
	ISistemaDAO dao;
	@Autowired
	IFinderSistema finder;
	
	@RequestMapping(value="/incluir", method=RequestMethod.GET)
	public String incluir (String nome) {
		Sistema sistema = new Sistema();
		sistema.setNome(nome);
		
		sistema = dao.incluir(sistema);
		
		return "";
	}
	
	@RequestMapping(value="/listar", method=RequestMethod.GET)
	public List<Sistema> listar () {
		List<Sistema> sistemas = finder.listar();
		return sistemas;
	}
	
	@RequestMapping(value="/pesquisar", method=RequestMethod.GET)
	public Sistema pesquisar (@RequestParam("id") long id) {
		Sistema sistema = new FinderSistemaImpl().pesquisar(id);
		return sistema;
	}
	
	
	@RequestMapping(value="/pesquisarPorNomeComo", method=RequestMethod.GET)
	public List<Sistema> pesquisarPorNomeComo (String nome) {
		List<Sistema> sistema = new FinderSistemaImpl().pesquisarPorNomeComo(nome);
		return sistema;
	}
	
	@RequestMapping(value="/excluir", method=RequestMethod.DELETE)
	public void excluir (long id) {
		Sistema o = finder.pesquisar(id);
		dao.excluir(o);
	}
	
}
