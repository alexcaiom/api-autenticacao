package br.com.waiso.autenticacao.controller.usuario.profissional;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.waiso.autenticacao.orm.dao.finder.impl.usuario.profissional.FinderEmpresaImpl;
import br.com.waiso.autenticacao.orm.dao.finder.usuario.profissional.IFinderEmpresa;
import br.com.waiso.autenticacao.orm.dao.usuario.profissional.IEmpresaDAO;
import br.com.waiso.autenticacao.orm.modelo.usuario.profissional.Empresa;

@Controller
@RequestMapping("/empresa")
public class EmpresaCRUD {

	@Autowired
	IEmpresaDAO dao;
	@Autowired
	IFinderEmpresa finderEmpresa;
	
	@RequestMapping(value="/incluir", method=RequestMethod.POST)
	public void incluir (String nome) {
		Empresa empresa = new Empresa();
		empresa.setNome(nome);
		empresa = dao.incluir(empresa);
	}
	
	@RequestMapping(value="/listar", method=RequestMethod.GET)
	public List<Empresa> listar () {
		List<Empresa> empresas = new FinderEmpresaImpl().listar();
		return empresas;
	}
	
	@RequestMapping(value="/pesquisar", method=RequestMethod.GET)
	public Empresa pesquisar (@RequestParam("id") Long id) {
		Empresa empresa = new FinderEmpresaImpl().pesquisar(id);
		return empresa;
	}
	
	@RequestMapping(value="/pesquisarPorNomeComo", method=RequestMethod.GET)
	public List<Empresa> pesquisarPorNomeComo (@RequestParam("nome") String nome) {
		List<Empresa> empresas = finderEmpresa.pesquisarPorNomeComo(nome);
		return empresas;
	}
	
}
