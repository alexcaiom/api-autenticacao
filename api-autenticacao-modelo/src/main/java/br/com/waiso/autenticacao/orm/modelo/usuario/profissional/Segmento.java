package br.com.waiso.autenticacao.orm.modelo.usuario.profissional;

import br.com.waiso.persistencia.jdbc.model.EntidadeModelo;

public class Segmento extends EntidadeModelo {

	private String nome;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	@Override
	public String toString() {
		return "Segmento [nome=" + nome + ", id=" + id + "]";
	}
	
}
