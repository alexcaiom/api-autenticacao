package br.com.waiso.autenticacao.orm.modelo.usuario.academico;

import br.com.waiso.persistencia.jdbc.model.EntidadeModelo;

public class AtividadeEscolar extends EntidadeModelo {

	private String nome;
	private String descricao;
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
}
