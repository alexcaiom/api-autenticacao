package br.com.waiso.autenticacao.orm.modelo.usuario;

import br.com.waiso.autenticacao.orm.modelo.Endereco;
import br.com.waiso.persistencia.jdbc.anotacoes.Tabela;
import br.com.waiso.persistencia.jdbc.model.EntidadeModelo;

@Tabela(nome="tbl_endereco_usuario")
public class EnderecoDoUsuario extends EntidadeModelo {
	
	private Usuario usuario;
	private Endereco endereco;
	
	public Usuario getUsuario() {
		return usuario;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	public Endereco getEndereco() {
		return endereco;
	}
	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}
	
}
