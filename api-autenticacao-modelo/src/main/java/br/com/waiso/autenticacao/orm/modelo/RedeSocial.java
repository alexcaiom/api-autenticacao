package br.com.waiso.autenticacao.orm.modelo;

import br.com.waiso.persistencia.jdbc.anotacoes.Tabela;
import br.com.waiso.persistencia.jdbc.model.EntidadeModelo;

@Tabela(nome="tbl_rede_social")
public class RedeSocial extends EntidadeModelo {

	private String nome;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
}
