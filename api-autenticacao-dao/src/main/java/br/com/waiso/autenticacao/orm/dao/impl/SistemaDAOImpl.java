package br.com.waiso.autenticacao.orm.dao.impl;

import org.springframework.stereotype.Component;

import br.com.waiso.autenticacao.orm.dao.ISistemaDAO;
import br.com.waiso.autenticacao.orm.modelo.sistema.Sistema;
import br.com.waiso.framework.exceptions.ErroUsuario;

@Component(value="ISistemaDAO")
public class SistemaDAOImpl extends DAO<Sistema>
							implements ISistemaDAO {

	public SistemaDAOImpl() {
		super(Sistema.class);
	}
	
	public Sistema incluir(Sistema o) throws ErroUsuario {
		return super.incluir(o);
	}

	public void editar(Sistema o) throws ErroUsuario {
		super.editar(o);
	}

	public void excluir(Sistema o) throws ErroUsuario {
		super.excluir(o);
	}

}
