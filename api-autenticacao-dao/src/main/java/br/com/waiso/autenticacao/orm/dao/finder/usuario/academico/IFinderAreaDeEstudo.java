package br.com.waiso.autenticacao.orm.dao.finder.usuario.academico;

import br.com.waiso.autenticacao.orm.modelo.usuario.academico.AreaDeEstudo;
import br.com.waiso.persistencia.interfaces.IFinder;

public interface IFinderAreaDeEstudo extends IFinder<Long, AreaDeEstudo> {
	
}
