package br.com.waiso.autenticacao.orm.dao.finder.usuario.profissional;

import br.com.waiso.autenticacao.orm.modelo.usuario.profissional.ExperienciaProfissional;
import br.com.waiso.persistencia.interfaces.IFinder;

public interface IFinderExperienciaProfissional extends IFinder<Long, ExperienciaProfissional> {
	
}
