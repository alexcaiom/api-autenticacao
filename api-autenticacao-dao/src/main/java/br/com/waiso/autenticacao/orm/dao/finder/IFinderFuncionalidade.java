package br.com.waiso.autenticacao.orm.dao.finder;

import br.com.waiso.autenticacao.orm.modelo.sistema.Funcionalidade;
import br.com.waiso.persistencia.interfaces.IFinder;

public interface IFinderFuncionalidade extends IFinder<Long, Funcionalidade> {
	
}
