package br.com.waiso.autenticacao.orm.dao.finder.usuario.profissional;

import java.util.List;

import br.com.waiso.autenticacao.orm.modelo.usuario.profissional.Empresa;
import br.com.waiso.persistencia.interfaces.IFinder;

public interface IFinderEmpresa extends IFinder<Long, Empresa> {
	
	public List<Empresa> pesquisarPorNomeComo(String nome);
	
}
