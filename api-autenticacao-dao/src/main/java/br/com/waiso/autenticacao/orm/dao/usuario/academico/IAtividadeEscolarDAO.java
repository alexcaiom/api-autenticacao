package br.com.waiso.autenticacao.orm.dao.usuario.academico;

import br.com.waiso.autenticacao.orm.modelo.usuario.academico.AtividadeEscolar;
import br.com.waiso.persistencia.interfaces.IDAO;

public interface IAtividadeEscolarDAO extends IDAO<AtividadeEscolar> {

}
