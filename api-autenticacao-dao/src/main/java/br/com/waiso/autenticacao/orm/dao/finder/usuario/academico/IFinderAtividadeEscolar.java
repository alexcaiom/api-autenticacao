package br.com.waiso.autenticacao.orm.dao.finder.usuario.academico;

import br.com.waiso.autenticacao.orm.modelo.usuario.academico.AtividadeEscolar;
import br.com.waiso.persistencia.interfaces.IFinder;

public interface IFinderAtividadeEscolar extends IFinder<Long, AtividadeEscolar> {
	
}
