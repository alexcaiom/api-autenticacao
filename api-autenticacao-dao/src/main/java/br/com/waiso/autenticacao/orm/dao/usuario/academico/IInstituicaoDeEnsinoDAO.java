package br.com.waiso.autenticacao.orm.dao.usuario.academico;

import br.com.waiso.autenticacao.orm.modelo.usuario.academico.InstituicaoDeEnsino;
import br.com.waiso.persistencia.interfaces.IDAO;

public interface IInstituicaoDeEnsinoDAO extends IDAO<InstituicaoDeEnsino> {

}
