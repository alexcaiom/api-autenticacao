package br.com.waiso.autenticacao.orm.dao.impl;

import br.com.waiso.autenticacao.orm.dao.IFuncionalidadeDAO;
import br.com.waiso.autenticacao.orm.modelo.sistema.Funcionalidade;
import br.com.waiso.framework.exceptions.ErroUsuario;

public class FuncionalidadeDAOImpl extends DAO<Funcionalidade>
							implements IFuncionalidadeDAO {

	public FuncionalidadeDAOImpl() {
		super(Funcionalidade.class);
	}
	
	public Funcionalidade incluir(Funcionalidade o) throws ErroUsuario {
		return super.incluir(o);
	}

	public void editar(Funcionalidade o) throws ErroUsuario {
		super.editar(o);
	}

	public void excluir(Funcionalidade o) throws ErroUsuario {
		super.excluir(o);
	}

}
