package br.com.waiso.autenticacao.orm.dao.finder;

import br.com.waiso.autenticacao.orm.modelo.sistema.Modulo;
import br.com.waiso.persistencia.interfaces.IFinder;

public interface IFinderModulo extends IFinder<Long, Modulo> {
	
}
