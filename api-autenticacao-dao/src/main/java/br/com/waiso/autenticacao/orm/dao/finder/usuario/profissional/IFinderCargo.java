package br.com.waiso.autenticacao.orm.dao.finder.usuario.profissional;

import java.util.List;

import br.com.waiso.autenticacao.orm.modelo.usuario.profissional.Cargo;
import br.com.waiso.persistencia.interfaces.IFinder;

public interface IFinderCargo extends IFinder<Long, Cargo> {

	public List<Cargo> pesquisarPorNomeComo(String nome);
	
}
