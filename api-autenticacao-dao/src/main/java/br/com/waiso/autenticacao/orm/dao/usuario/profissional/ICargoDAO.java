package br.com.waiso.autenticacao.orm.dao.usuario.profissional;

import br.com.waiso.autenticacao.orm.modelo.usuario.profissional.Cargo;
import br.com.waiso.persistencia.interfaces.IDAO;

public interface ICargoDAO extends IDAO<Cargo> {

}
