package br.com.waiso.autenticacao.orm.dao.finder.impl;

import br.com.waiso.autenticacao.orm.dao.impl.DAO;
import br.com.waiso.persistencia.jdbc.finder.FinderJDBC;

public abstract class Finder<T> extends FinderJDBC<T> {

	public Finder(Class<T> entidade) {
		super(entidade);
		DAO.configurarBancoDeDadosDadosAcesso();
	}

}
